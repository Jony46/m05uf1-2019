package com.itb2019;

import java.util.Arrays;

public class Main {

    TODO: canviar DWEEK a DIES_SETMANA
    static final int DWEEK = 7;
    TODO: Canviar DLABORABLES a DIES_LABORABLES
    static final int DLABORABLES = 5;
    static Textos textos;


    public static void main(String[] args) {
        System.out.println("args = " + Arrays.deepToString(args));
        System.out.println("-----------------------------------");

        textos = new Textos();
        if(args.length == 0){
            //  TODO: mostrar textos.getNo_arguments() (Primer fer els TODO de Textos)
            System.out.println("- Sense arguments");
            System.exit(0);
        }

        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            TODO: Controlar els idiomes ESPANYOL i INGLES
            switch (args[i]) {
                case Constants.CATALA:
                    System.out.println(textos.getDiesLaborals1CA() + DLABORABLES + textos.getDiesLaborals2CA());
                    break;
                default:
                    TODO: Mostrar textos.getNo_controlat();
                    System.out.println("Idioma no reconegut");
            }
        }

        System.out.println("\n--------- UNA ALTRA OPCIÓ: ---------");

        for (int i = 0; i < args.length; i++) {
            System.out.println("\n* " + args[i] + ":");
            System.out.println(textos.fraseDiesLaborals(args[i], DLABORABLES));
        }
    }

}
