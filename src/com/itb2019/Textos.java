package com.itb2019;

public class Textos<TODO, getter> {

    public Textos() {
    }

    private String diesLaborals1CA = "La setmana te ";
    private String diesLaborals2CA = "dies laborals.";

    private String diesLaborals1ES = "La semana tiene ";
    private String diesLaborals2ES = "dias laborales.";

    private String diesLaborals1EN = "The week has ";
    private String diesLaborals2EN = "working days.";

    private String no_controlat = "[!] Idioma no controlat";

    private String no_arguments = "No s'han indicat arguments";
    TODO: generar el getter


    public String getDiesLaborals1CA() {
        return diesLaborals1CA;
    }

    public String getDiesLaborals2CA() {
        return diesLaborals2CA;
    }

    public String getDiesLaborals1ES() {
        return diesLaborals1ES;
    }

    public String getDiesLaborals2ES() {
        return diesLaborals2ES;
    }

    public String getDiesLaborals1EN() {
        return diesLaborals1EN;
    }

    public String getDiesLaborals2EN() {
        return diesLaborals2EN;
    }

    public String getNo_controlat() {
        return no_controlat;
    }

    public String fraseDiesLaborals(String idioma, int diesLaborals) {
        String frase = no_controlat;

        switch (idioma) {
            case Constants.ESPANYOL:
                frase = diesLaborals1ES + diesLaborals + Constants.ESPAI + diesLaborals2ES;
                break;
            TODO: Afegir control per altres idiomes CA i EN
            default:
                break;
        }
        return frase;

    }

}
